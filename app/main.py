from datetime import datetime
from typing import List, Optional

import uvicorn
from fastapi import FastAPI, Query

from app.configs.database import MONGODB
from app.extract.ethplorer import extract_single_address, get_addresses
from app.models import PubSubEvent, WalletAddress, WalletNotional
from app.transform.ethplorer import download_file_as_text, transform_data

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Go to /docs to view the api documentation for more information"}


@app.get("/track_wallet", response_model=List[WalletAddress])
async def get_wallets_tracked() -> List[WalletAddress]:
    """Get list of wallets in MongoDB `wallet_addresses` table"""

    return MONGODB.table_wallet_addresses.find({"_id": 0})


@app.post("/track_wallet", response_model=WalletAddress)
async def add_wallet_to_track(
    wallet_address: str,
    description: Optional[str] = None,
) -> str:
    """
    To add one wallet address to MongoDB `wallet_addresses` table
    """

    try:
        data = WalletAddress(
            created_at_utc=datetime.now(),
            wallet=wallet_address,
            description=description,
        )
        collection = MONGODB.table_wallet_addresses
        collection.insert_one(data.dict())

        return data

    except Exception as e:
        return f"Error {e}"


@app.post("/extract_ethplorer")
async def extract_ethplorer_data(timestamp: str) -> dict:
    """
    To extract ethplorer data for addresses in MongoDB `wallet_addresses`
    table and save to Google Cloud Storage (GCS)
    """
    completed = []
    addresses = get_addresses()
    for address in addresses:
        extract_single_address(address, timestamp)
        completed.append(address)

    return {"completed": completed}


@app.post("/transform_ethplorer", response_model=List[WalletNotional])
async def transform_ethplorer_data(body_msg: PubSubEvent) -> List[WalletNotional]:
    """
    Transform raw ethplorer data and save to MongoDB `wallets_notional` table
    """
    logs_collection = MONGODB.db.ethplorer_pubsub_logs
    logs_collection.insert_one(body_msg.dict())

    bucket = body_msg.message.attributes.bucketId
    filepath = body_msg.message.attributes.objectId
    object = download_file_as_text(bucket=bucket, filepath=filepath)
    transformed_data = transform_data(object, filepath)

    wallets_notional_collection = MONGODB.db.wallets_notional
    for record in transformed_data:
        # Convert decimal to str
        record_dict = record.dict()
        for decimal_col in ["asset_price", "asset_balance", "asset_notional_usd"]:
            record_dict[decimal_col] = str(record_dict[decimal_col])
        wallets_notional_collection.insert_one(record_dict)

    return transformed_data


@app.get("/retrieve_wallet_notional", response_model=List[WalletNotional])
async def retrieve_wallet_notional(
    start_date_utc: Optional[datetime] = None,
    end_date_utc: Optional[datetime] = None,
    assets: Optional[List[str]] = Query(None),
    addresses: Optional[List[str]] = Query(None),
    limit: int = 1000,
    skip: int = 0,
) -> List[WalletNotional]:
    """
    Retrieve wallet notional with the option of the following query parameters:

        1. start_date_utc
            description: start date of when data is first received
            type: string
            format: yyyy-mm-ddTHH:MM:SS (e.g. 2008-09-15T15:53:00)
        2. end_date_utc
            description: end date of when data is first received
            type: string
            format: yyyy-mm-ddTHH:MM:SS (e.g. 2008-09-15T15:53:00)
        3. assets
            description: list of asset symbols to retrieve (e.g. eth)
            type: list of string
        4. addresses
            description: list of wallet addresses to retrieve
            type: list of string
        5. limit
            description: limit n number of results, defaults to 1000, max of 1000
            type: int
        6. skip
            description: skip n number of results, defaults to 0
            type: int

    """
    limit = min(1000, limit)

    filters = {}
    if start_date_utc:
        filters["retrieved_at_utc"] = {"$gte": start_date_utc}  # type: ignore
    if end_date_utc:
        filters["retrieved_at_utc"] = {"$lte": end_date_utc}  # type: ignore
    if assets:
        assets = [asset.lower() for asset in assets]
        filters["asset_symbol"] = {"$in": assets}  # type: ignore
    if addresses:
        addresses = [address.lower() for address in addresses]
        filters["address"] = {"$in": addresses}  # type: ignore

    wallets_notional_collection = MONGODB.db.wallets_notional
    return wallets_notional_collection.find(filters, {"_id": 0}).skip(skip).limit(limit)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8004)
