import json
import logging
import os
from datetime import datetime, timezone
from decimal import Decimal
from typing import List

from google.cloud.storage import Client

from app.configs.environment_var import GCS_SERVICE_ACCOUNT
from app.models import WalletNotional

logging.getLogger().setLevel(os.getenv("LOG_LEVEL", logging.INFO))
storage_client = Client.from_service_account_json(GCS_SERVICE_ACCOUNT)
CHAINNAME = "Ethereum Mainnet"


def download_file_as_text(bucket: str, filepath: str) -> dict:
    bucket = storage_client.get_bucket(bucket)
    blob = bucket.get_blob(filepath)  # type: ignore
    return json.loads(blob.download_as_text(encoding="utf-8"))


def get_retrieved_at_utc(filepath: str) -> datetime:
    datetime_items = filepath.rstrip(".json").split("/")[-4:]
    year = int(datetime_items[0].replace("year=", ""))
    month = int(datetime_items[1].replace("month=", ""))
    day = int(datetime_items[2].replace("day=", ""))
    hour = int(datetime_items[3][:2])
    minute = int(datetime_items[3][2:4])
    second = int(datetime_items[3][4:6])
    ms = int(float(datetime_items[3][6:]))
    return datetime(year, month, day, hour, minute, second, ms, tzinfo=timezone.utc)


def get_eth_data(raw_data: dict, retrieved_at_utc: datetime) -> WalletNotional:
    balance = Decimal(raw_data["ETH"]["balance"])
    price = Decimal(raw_data["ETH"]["price"]["rate"])

    return WalletNotional(
        address=raw_data["address"],
        chain_name=CHAINNAME,
        asset_class="coin",
        asset_name="Ethereum",
        asset_symbol="eth",
        asset_balance=balance,
        asset_price=price,
        asset_notional_usd=balance * price,
        retrieved_at_utc=retrieved_at_utc,
        created_at_utc=datetime.now(tz=timezone.utc),
    )


def get_tokens_data(raw_data: dict, retrieved_at_utc: datetime) -> List[WalletNotional]:
    wallets_notional = []
    for token in raw_data["tokens"]:
        try:
            tokeninfo = token["tokenInfo"]
            balance = Decimal(token["balance"]) / (10 ** Decimal(tokeninfo["decimals"]))
            price = tokeninfo["price"]
            is_price_present = tokeninfo["price"] is not False
            if is_price_present:
                price = Decimal(price["rate"])
                nominal = price * balance
            else:
                price = nominal = None

            wallet_nominal = WalletNotional(
                address=raw_data["address"],
                chain_name=CHAINNAME,
                asset_class="token",
                asset_name=tokeninfo["name"],
                asset_symbol=tokeninfo["symbol"].lower(),
                asset_balance=balance,
                asset_price=price,
                asset_notional_usd=nominal,
                retrieved_at_utc=retrieved_at_utc,
                created_at_utc=datetime.now(tz=timezone.utc),
            )
            wallets_notional.append(wallet_nominal)
        except Exception as e:
            logging.error("Failed to retrieve")
            logging.error(e)
            logging.error(token)
    return wallets_notional


def transform_data(raw_data: dict, filepath: str) -> List[WalletNotional]:
    retrieved_at_utc = get_retrieved_at_utc(filepath)
    eth_data = get_eth_data(raw_data, retrieved_at_utc)
    tokens = get_tokens_data(raw_data, retrieved_at_utc)
    return tokens + [eth_data]
