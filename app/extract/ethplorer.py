import json
import logging
import os
from datetime import datetime, timezone
from typing import List

import requests
from google.cloud.storage import Client
from requests.models import Response

from app.configs.database import MONGODB
from app.configs.environment_var import ETHPLORER_API, GCS_SERVICE_ACCOUNT

logging.getLogger().setLevel(os.getenv("LOG_LEVEL", logging.INFO))
storage_client = Client.from_service_account_json(GCS_SERVICE_ACCOUNT)
RAW_DATETIME_FORMAT = "year=%Y/month=%m/day=%d/%H%M%S%f"
RAW_FILE_PREFIX = "extract/ethplorer"


def get_addresses() -> List[str]:
    return [item["wallet"] for item in MONGODB.table_wallet_addresses.find()]


def get_filepath(retrieved_at_utc: datetime) -> str:
    retrieved_at_utc_str = datetime.strftime(
        retrieved_at_utc,
        RAW_DATETIME_FORMAT,
    )
    return f"{RAW_FILE_PREFIX}/{retrieved_at_utc_str}.json"


def retrieve_address_data(address: str, api_key=ETHPLORER_API) -> Response:
    """Retrieve wallet address data from ethplorer api"""

    url = f"https://api.ethplorer.io/getAddressInfo/{address}"

    return requests.get(url, params=dict(apiKey=api_key))


def upload_blob(bucket: str, retrieved_at_utc: datetime, response: Response):
    """
    Save wallet address data to GCS
    """

    filepath = get_filepath(retrieved_at_utc)

    if response.status_code != 200:
        logging.warn(
            f"Ethplorer response failed at {retrieved_at_utc}: {response.status_code}"
        )
    else:
        bucket = storage_client.get_bucket(bucket)
        blob = bucket.blob(filepath)  # type: ignore
        blob.upload_from_string(
            data=json.dumps(response.json()), content_type="application/json"
        )
        logging.info(f"Saved to gs://{bucket}/{blob.name}")


def extract_single_address(wallet_address: str, timestamp: str):
    retrieved_at_utc = datetime.now(tz=timezone.utc)
    response = retrieve_address_data(wallet_address)
    upload_blob("polychain-test", retrieved_at_utc, response)
