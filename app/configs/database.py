from dataclasses import dataclass

from pymongo import MongoClient

from app.configs.environment_var import DB_NAME, DB_PASSWORD, DB_USER, MONGODB_CLUSTER


@dataclass
class MongoDB:
    client = MongoClient(
        f"mongodb+srv://{DB_USER}:{DB_PASSWORD}"
        f"@{MONGODB_CLUSTER}/{DB_NAME}?retryWrites=true&w=majority"
    )

    @property
    def db(self):
        return getattr(self.client, DB_NAME)

    @property
    def table_wallet_addresses(self):
        return getattr(self.client, DB_NAME).wallet_addresses


MONGODB = MongoDB()
