import os
from pathlib import Path

import dotenv

BASE_DIR = Path(__file__).resolve().parent.parent.parent
env_file = os.path.join(BASE_DIR, ".env")

if dotenv.find_dotenv(env_file):
    dotenv.load_dotenv(env_file)

ENVIRONMENT = os.getenv("ENVIRONMENT", default="")
ETHPLORER_API = os.getenv("ETHPLORER_API", default="")
DB_NAME = os.getenv("DB_NAME", default="")
DB_USER = os.getenv("DB_USER", default="")
DB_PASSWORD = os.getenv("DB_PASSWORD", default="")
MONGODB_CLUSTER = os.getenv("MONGODB_CLUSTER", default="")
LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", default="")
GCS_SERVICE_ACCOUNT = os.getenv("GCS_SERVICE_ACCOUNT", default="")
