from datetime import datetime
from decimal import Decimal
from typing import Optional

from pydantic import BaseModel


class WalletAddress(BaseModel):
    created_at_utc: datetime
    wallet: str
    description: str


class PubSubMessageAttribute(BaseModel):
    bucketId: str
    eventTime: str
    eventType: str
    notificationConfig: str
    objectGeneration: str
    objectId: str
    payloadFormat: str


class PubSubMessage(BaseModel):
    attributes: PubSubMessageAttribute
    data: str
    messageId: str
    message_id: str
    publishTime: str
    publish_time: str


class PubSubEvent(BaseModel):
    message: PubSubMessage
    subscription: str


class WalletNotional(BaseModel):
    address: str
    chain_name: str
    asset_class: str
    asset_name: str
    asset_symbol: str
    asset_balance: Decimal
    asset_price: Optional[Decimal]
    asset_notional_usd: Optional[Decimal]
    retrieved_at_utc: datetime
    created_at_utc: datetime
