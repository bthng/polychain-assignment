setup-test: run-dev test

help: ## Get a description of what each command does
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

build-dev: ## Build docker image for development
	docker build --pull --tag polychainweb --target dev_web --rm --file Dockerfile  .

run-dev: ## Run dev container
	docker run --name polychainweb -d -p 8004:8004 -e PORT=8004 --rm polychainweb

stop-dev: ## Stop dev container
	docker stop polychainweb

test: ## Run test locally
	poetry run python -m pytest -vv tests/
