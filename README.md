# Local Development

<!-- GETTING STARTED -->
### Prerequisites

Before you begin installation, you're recommended to have the following
* [asdf](https://github.com/asdf-vm/asdf)
  ```sh
  brew install asdf
  ```
* [docker](https://docs.docker.com/docker-for-mac/install/)

### Installation

1. Clone the repo
2. Install [poetry](https://python-poetry.org/docs/) and python with asdf
   ```sh
   asdf plugin-add poetry https://github.com/asdf-community/asdf-poetry.git
   asdf install
   ```
3. Install dev dependencies
   ```sh
   poetry install
   ```

# Manual Deployment
<!-- This will be replaced by CI/CD eventually -->
## Step 1: Build and push image
```sh
docker build --pull --tag $IMAGE_URL --target production_web --file Dockerfile  .
docker push "$IMAGE_URL"
```

## Step 2: Deploy image
```sh
gcloud run deploy polychain-production-web --image $IMAGE_URL
```

# Documentation

* More information on this pipeline can be found in [docs/data_ingestion_pipeline.md](docs/data_ingestion_pipeline.md)
* More information on the API to retrieve data can be found in [docs/api_design.md](docs/api_design.md)
