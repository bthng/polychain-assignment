from datetime import datetime, timezone

import pytest
from freezegun import freeze_time

from app.models import WalletNotional
from app.transform.ethplorer import (
    CHAINNAME,
    get_eth_data,
    get_retrieved_at_utc,
    get_tokens_data,
    transform_data,
)


@pytest.fixture
def example_filepath() -> str:
    return "extract/ethplorer/year=2021/month=12/day=18/235651888888.json"


def test_get_retrieved_at_utc(example_filepath):
    assert get_retrieved_at_utc(example_filepath) == datetime(
        2021, 12, 18, 23, 56, 51, 888888, tzinfo=timezone.utc
    )


@pytest.fixture
def example_ethplorer_raw() -> dict:
    return {
        "address": "address.eth",
        "ETH": {
            "price": {
                "rate": 3000,
                "diff": 2.16,
                "diff7d": -2.85,
                "ts": 1639871523,
                "marketCapUsd": 471600039340.58167,
                "availableSupply": 118795047.9365,
                "volume24h": 19605880383.405037,
                "diff30d": -4.207390659779648,
                "volDiff1": -14.469696554739784,
                "volDiff7": -7.291607544534003,
                "volDiff30": 16.60221921920983,
            },
            "balance": 1,
            "rawBalance": "1",
        },
        "countTxs": 0,
        "tokens": [
            {
                "tokenInfo": {
                    "address": "tokenaddress",
                    "name": "Matic Network",
                    "decimals": "18",
                    "symbol": "MATIC",
                    "totalSupply": "10000000000000000000000000000",
                    "owner": "",
                    "lastUpdated": 1639871026,
                    "slot": 0,
                    "issuancesCount": 0,
                    "holdersCount": 306969,
                    "image": "/images/MATIC7d1afa7b.png",
                    "website": "https://matic.network",
                    "telegram": "https://t.me/maticnetwork",
                    "twitter": "maticnetwork",
                    "reddit": "maticnetwork",
                    "coingecko": "matic-network",
                    "ethTransfersCount": 0,
                    "price": {
                        "rate": 2.23,
                        "diff": 4.84,
                        "diff7d": -22.19,
                        "ts": 1639871566,
                        "marketCapUsd": 0,
                        "availableSupply": 0,
                        "volume24h": 1613171812.2843664,
                        "diff30d": 42.19754635071513,
                        "volDiff1": -26.74763303069807,
                        "volDiff7": -48.15810833738171,
                        "volDiff30": 72.89714540239441,
                        "currency": "USD",
                    },
                },
                "balance": 4e18,
                "totalIn": 0,
                "totalOut": 0,
                "rawBalance": "4000000000000000000",
            }
        ],
    }


@freeze_time("Dec 19th, 2021")
def test_get_eth_data(example_ethplorer_raw, monkeypatch):
    retrieved_at_utc = datetime(2021, 12, 18, 23, 56, 51, tzinfo=timezone.utc)

    def mock_created_at_utc():
        return datetime(2021, 12, 19, 0, 0, 0, tzinfo=timezone.utc)

    assert get_eth_data(example_ethplorer_raw, retrieved_at_utc) == WalletNotional(
        address="address.eth",
        chain_name=CHAINNAME,
        asset_class="coin",
        asset_name="Ethereum",
        asset_symbol="eth",
        asset_balance=1,
        asset_price=3000,
        asset_notional_usd=3000,
        retrieved_at_utc=retrieved_at_utc,
        created_at_utc=mock_created_at_utc(),
    )


@freeze_time("Dec 19th, 2021")
def test_get_tokens_data(example_ethplorer_raw):
    retrieved_at_utc = datetime(2021, 12, 18, 23, 56, 51, 888888, tzinfo=timezone.utc)

    def mock_created_at_utc():
        return datetime(2021, 12, 19, 0, 0, 0, tzinfo=timezone.utc)

    assert get_tokens_data(example_ethplorer_raw, retrieved_at_utc) == [
        WalletNotional(
            address="address.eth",
            chain_name=CHAINNAME,
            asset_class="token",
            asset_name="Matic Network",
            asset_symbol="matic",
            asset_balance=4,
            asset_price=2.23,
            asset_notional_usd=8.92,
            retrieved_at_utc=retrieved_at_utc,
            created_at_utc=mock_created_at_utc(),
        )
    ]


@freeze_time("Dec 19th, 2021")
def test_transform_data(example_ethplorer_raw, example_filepath):
    retrieved_at_utc = datetime(2021, 12, 18, 23, 56, 51, 888888, tzinfo=timezone.utc)

    def mock_created_at_utc():
        return datetime(2021, 12, 19, 0, 0, 0, tzinfo=timezone.utc)

    assert transform_data(example_ethplorer_raw, example_filepath) == [
        WalletNotional(
            address="address.eth",
            chain_name=CHAINNAME,
            asset_class="token",
            asset_name="Matic Network",
            asset_symbol="matic",
            asset_balance=4,
            asset_price=2.23,
            asset_notional_usd=8.92,
            retrieved_at_utc=retrieved_at_utc,
            created_at_utc=mock_created_at_utc(),
        ),
        WalletNotional(
            address="address.eth",
            chain_name=CHAINNAME,
            asset_class="coin",
            asset_name="Ethereum",
            asset_symbol="eth",
            asset_balance=1,
            asset_price=3000,
            asset_notional_usd=3000,
            retrieved_at_utc=retrieved_at_utc,
            created_at_utc=mock_created_at_utc(),
        ),
    ]
