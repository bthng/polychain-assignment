from datetime import datetime

import pytest
from pydantic.error_wrappers import ValidationError

from app.models import (
    PubSubEvent,
    PubSubMessage,
    PubSubMessageAttribute,
    WalletNotional,
)


class TestPubSubMessageAttribute:
    def test_valid_pubsub_message_attribute(self):
        data = {
            "bucketId": "value",
            "eventTime": "value",
            "eventType": "value",
            "notificationConfig": "value",
            "objectGeneration": "value",
            "objectId": "value",
            "payloadFormat": "value",
        }

        attribute = PubSubMessageAttribute(**data)
        assert attribute.dict() == data

    def test_invalid_pubsub_message_attribute(self):
        data = {
            "bucketId": "value",
            "eventTime": "value",
        }
        with pytest.raises(ValidationError) as exc_info:
            _ = PubSubMessageAttribute(**data)
            assert "value_error.missing" in str(exc_info.value)


class TestPubSubMessage:
    def test_valid_pubsub_message(self):
        attribute = {
            "bucketId": "value",
            "eventTime": "value",
            "eventType": "value",
            "notificationConfig": "value",
            "objectGeneration": "value",
            "objectId": "value",
            "payloadFormat": "value",
        }

        data = {
            "data": "data",
            "attributes": attribute,
            "messageId": "foo",
            "message_id": "foo",
            "publishTime": "2",
            "publish_time": "2",
        }

        attribute = PubSubMessage(**data)
        assert attribute.dict() == data

    def test_invalid_pubsub_message(self):
        data = {
            "invalid": "value",
        }
        with pytest.raises(ValidationError) as exc_info:
            _ = PubSubMessage(**data)
            assert "value_error.missing" in str(exc_info.value)


class TestPubSubEvent:
    def test_valid_pubsub_event(self):
        attribute = {
            "bucketId": "value",
            "eventTime": "value",
            "eventType": "value",
            "notificationConfig": "value",
            "objectGeneration": "value",
            "objectId": "value",
            "payloadFormat": "value",
        }

        message = {
            "data": "data",
            "attributes": attribute,
            "messageId": "foo",
            "message_id": "foo",
            "publishTime": "2",
            "publish_time": "2",
        }

        data = {
            "message": message,
            "subscription": "subscription",
        }

        attribute = PubSubEvent(**data)
        assert attribute.dict() == data

    def test_invalid_pubsub_event(self):
        data = {
            "invalid": "value",
        }
        with pytest.raises(ValidationError) as exc_info:
            _ = PubSubEvent(**data)
            assert "value_error.missing" in str(exc_info.value)


class TestWalletNotional:
    def test_valid_wallet_nominal(self):
        data = {
            "address": "0x123456789",
            "chain_name": "Ethereum Mainnet",
            "asset_class": "token",
            "asset_name": "Ethereum",
            "asset_symbol": "eth",
            "asset_balance": 10,
            "asset_price": 1,
            "asset_notional_usd": 10.0,
            "retrieved_at_utc": datetime(2021, 1, 1),
            "created_at_utc": datetime(2021, 1, 1),
        }

        attribute = WalletNotional(**data)
        assert attribute.dict() == data

    def test_invalid_wallet_nominal(self):
        data = {
            "invalid": "value",
        }
        with pytest.raises(ValidationError) as exc_info:
            _ = WalletNotional(**data)
            assert "value_error.missing" in str(exc_info.value)

    def test_invalid_data_type_wallet_nominal(self):
        data = {
            "address": "value",
            "asset_class": "value",
            "asset_name": "value",
            "asset_balance": "value",
            "asset_price": "value",
            "asset_notional_usd": "value",
            "retrieved_at_utc": "",
            "created_at_utc": "",
        }
        with pytest.raises(ValidationError) as exc_info:
            _ = WalletNotional(**data)
            assert "value is not a valid" in str(exc_info.value)
