def test_root(test_client):
    response = test_client.get("/")
    message = response.json()["message"]

    assert response.status_code == 200
    assert message == "Go to /docs to view the api documentation for more information"
