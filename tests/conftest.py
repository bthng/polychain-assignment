import time
from typing import Generator, Optional

import pytest
import requests
from fastapi.testclient import TestClient


def wait_for_webapp_to_come_up() -> Optional[requests.Response]:
    wait_till = time.time() + 10
    url = "http://localhost:8004"
    while time.time() < wait_till:
        try:
            return requests.get(url)
        except requests.exceptions.ConnectionError:
            time.sleep(0.5)
    pytest.fail("API is not running")
    return None


@pytest.fixture(scope="session")
def test_client() -> Generator[TestClient, None, None]:
    from app.main import app

    wait_for_webapp_to_come_up()
    yield TestClient(app)
