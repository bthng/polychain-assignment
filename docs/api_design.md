# API Design

The API is currently deployed [here](https://polychain-production-web-icnclcsnqq-ue.a.run.app//docs).
Users will use most oftenly use the `POST /track_wallet` and `GET /retrieve_wallet_notional` endpoints.


## `POST /track_wallet`
This endpoint allows users to add new wallets to track.

Example curl command:
```sh
curl -X 'POST' \
  'https://polychain-production-web-icnclcsnqq-ue.a.run.app/track_wallet?wallet_address=0x123123&description=test%20wallet%20123' \
  -H 'accept: application/json' \
  -d ''
```

Example response:
```
{
  "created_at_utc": "2021-12-20T05:45:58.901Z",
  "wallet": "string",
  "description": "string"
}
```


## `GET /retrieve_wallet_notional`
This endpoint allows users to get the wallet notional. It is filterable by time, assets and addresses.

Example curl command to get all ETH and USDC related assets:
```sh
curl -X 'GET' \
  'https://polychain-production-web-icnclcsnqq-ue.a.run.app/retrieve_wallet_notional?assets=eth&assets=usdc' \
  -H 'accept: application/json'
```

Example response:
```
[
    {
        "_id": "61bf8bc34d8a2723d758ba8f",
        "address": "0x2faf487ah2222277222222222222222222222222",
        "chain_name": "Ethereum Mainnet",
        "asset_class": "token",
        "asset_name": "USD Coin",
        "asset_symbol": "usdc",
        "asset_balance": "17445787.150869",
        "asset_price": "0.99993171385592305444589555918355472385883331298828125",
        "asset_notional_usd": "17444595.84533408003398175225",
        "retrieved_at_utc": "2021-12-19T19:00:06.164000",
        "created_at_utc": "2021-12-19T19:44:54.652000"
    },
    {
        "_id": "61bf8ce00c3b830d1f9af697",
        "address": "0x2faf483333333333333333333333333333333333",
        "chain_name": "Ethereum Mainnet",
        "asset_class": "token",
        "asset_name": "USD Coin",
        "asset_symbol": "usdc",
        "asset_balance": "17445787.150869",
        "asset_price": "0.99993171385592305444589555918355472385883331298828125",
        "asset_notional_usd": "17444595.84533408003398175225",
        "retrieved_at_utc": "2021-12-19T19:00:06.164000",
        "created_at_utc": "2021-12-19T19:49:37.682000"
    },
]
```
