# Data Ingestion Pipeline

This documentation will briefly describe the ETL pipeline of the service in this repository. I have deployed this pipeline on Google [Cloud Run](https://cloud.google.com/run).

## Step 1: Extract data from third-party APIs

### Description
The POST `extract_ethplorer` endpoint will be called every 30 minutes scheduled using [Cloud Scheduler](https://cloud.google.com/scheduler/docs/quickstart).
This endpoint will extract data from third-party APIs (i.e. ethplorer), and will be saved in a GCS bucket.
Whenever a new file has been added to the GCS bucket, it will trigger a [Pub/Sub](https://cloud.google.com/pubsub/docs) notification.

### Rationale

#### Why 30 minutes:

The data extraction was initially scheduled for every 5 minutes due to rate limits of the API used. It was later changed to 30 minutes intervals instead to save on storage cost. I am also assuming that the wallets will not be too active and a 30 minutes interval will be sufficient for the use case. Overtie we can also identify wallets that are more active, and perhaps move them to a separate pipeline that runs more frequently (i.e. every minute) should we need that granularity.

#### Why GCS:

I have also chosen to store the raw data on cloud storage. This is to make the pipeline idempotent. This allows me to rerun any preprocessing jobs in the event that the transform step fails, or if I choose to amend my transform logic. It is also cheaper to store the raw data on cloud storage than in a database.

#### Why Ethplorer.io:

I have chosen [ethplorer.io](ethplorer.io) to collect data related to Ethereum mainnet assets. The main reason why I have chosen ethplorer.io for this assignment is because the get-address-info endpoint allowed me to retrieve the list of all tokens in the wallet, which was helpful in helping me collect the final nominal USD amount. Ethplorer also includes the price balance of coingecko, which is a very popular source of coin pricing data, and hence I decided that it is reliable. If coingecko is an unreliable source, I can also update the logic to use an alternative data provider like CoinMarketCap or even an exchange like FTX/Binance.

##### Alternatives considered:

[Etherscan.io](https://etherscan.io/apis) - This was originally my first choice but it required a lot of preprocessing. It also required me to pull for an alternative coin price data provider (i.e. coingecko). The etherscan api requires the inclusion of specific contract addresses to retrieve the token balance of a specific coin which can be time consuming for a wallet with a very large number of coins, and I may miss out on pulling the data for all tokens. Additionally after taking into account the 5 calls/second
limit for the free api, I went ahead with ethplorer for simplicity and efficiency.



## Step 2: Transform data from third-party APIs

### Description
The Pub/Sub notification will trigger the POST `transform_ethplorer` endpoint.
This will then transform and save the data in the MongoDB `wallets_notional` table.

### Rationale

#### Why MongoDB:

I felt that a NoSQL database will be more suitable for this use case as I had to design an API and NoSQL typically has faster reads and writes compared to a RDBMS. I chose MongoDB specifically for the free tier database offered and its ease of use.

While it wasn't implemented in the current code, as the pipeline grows to be more stable and used increasingly, we can take further steps to optimise reads and writes to the database. Some optimisation techniques here includes sharding and addition of indexes. I left it out in this current implementation.

#### Schema Design:
I considered creating a nested schema just like how it was done with ethplorer - The tokens are nested in a single struct, and the ETH coin was in its own struct. However, for simplicity, I have decided to go with a flat schema as it’d be easier for users to work with. The requirements in the assignment also asked for the api to support (over a provided time frame, over a subset of assets, subset of addresses) - as such a flat schema will be better as there is less processing involved Here I made the assumptions that users will not too concerned about whether it is a coin or a token.

The `chain_name` field is kept to allow for multi chain ingestion in the future.

##### Schema of `wallets_notional` table:

| Field name           | Data Type | Description                                               | Example            |
| -------------------- | --------- | --------------------------------------------------------- | ------------------ |
| address              | STRING    | Wallet address                                            | 0x1223231231321232 |
| chain\_name          | STRING    | Name of chain                                             | Ethereum Mainet    |
| asset\_class         | STRING    | Asset class (coin or token)                               | coin               |
| asset\_name          | STRING    | Name of asset                                             | Ethereum           |
| asset\_symbol        | STRING    | Symbol of asset                                           | eth                |
| asset\_balance       | NUMERIC   | Amount of asset                                           | 2                  |
| asset\_price         | NUMERIC   | Price of asset                                            | 3000               |
| asset\_notional\_usd | NUMERIC   | Notional USD of asset                                     | 6000               |
| retrieved\_at\_utc   | DATETIME  | Timestamp of when data was retrieved from third-party API | 2021-01-01T000000  |
| created\_at\_utc     | DATETIME  | Timestamp of when data was inserted to database           | 2021-01-01T000022  |


### Improvements
In the interest of time, I have not implemented certain features that will be important for a proper productionised pipeline. Here I want to list a few of those considerations that I left out intentionally:
- Security of pipeline and database
- Error handling
- CI/CD pipeline for automatic deployment and tests
- Unit tests and integration tests
